<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();

Route::get('login', function () {
    return view('admin.auth.login');
})->name('login');
// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/dashboard', [App\Http\Controllers\HomeController::class, 'index'])->name('dashboard');
Route::get('/mitra', [App\Http\Controllers\MitraController::class, 'index'])->name('mitra');
Route::post('/mitra/store', [App\Http\Controllers\MitraController::class, 'store'])->name('mitra.store');
Route::post('/mitra/update', [App\Http\Controllers\MitraController::class, 'upd'])->name('mitra.update');


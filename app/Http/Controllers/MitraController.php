<?php

namespace App\Http\Controllers;

use App\Models\mitra;
use Illuminate\Http\Request;

class MitraController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        $mitra = mitra::get();

        return view('admin.mitra.index',compact('mitra'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */

    function incrementReferralCode($code)
    {
        $numberPart = intval(substr($code, 0, 3));
        $alphabetPart = substr($code, 3, 2);
        $yearPart = substr($code, -4);
    
        $numberPart++;
    
        if ($numberPart > 999) {
            $numberPart = 1;
            $yearPart = intval($yearPart) + 1;
        }
    
        $numberPartPadded = str_pad($numberPart, 3, '0', STR_PAD_LEFT);
        $newCode = $numberPartPadded . $alphabetPart . $yearPart;
    
        return $newCode;
    }
    
    function generateNextReferralCode($currentCode)
    {
        // Menaikkan $currentCode ke kode berikutnya
        $nextCode = $this->incrementReferralCode($currentCode);

        return $nextCode;
    }

    public function store(Request $request)
    {
        //
        $data = Mitra::latest()->first();

        $mitra = new mitra();
        $mitra->nama = $request->nama;
        // $mitra->ref_code = $request->ref_code;
        $mitra->ref_code = $this->generateNextReferralCode($data->ref_code);
        $mitra->link_group = $request->link_group;
        $mitra->save();

        $notify[] = ['success', 'Tambah data berhasil.'];
        return redirect()->back()->withNotify($notify);
    }

    /**
     * Display the specified resource.
     */
    public function show(mitra $mitra)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(mitra $mitra)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, mitra $mitra)
    {
        //

    }

    public function upd(Request $request){
        
        $mitra = mitra::where('id',$request->id)->first();
        $mitra->nama = $request->nama;
        $mitra->ref_code = $request->ref_code;
        $mitra->link_group = $request->link_group;
        $mitra->save();

        $notify[] = ['success', 'Edit data berhasil.'];
        return redirect()->back()->withNotify($notify);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(mitra $mitra)
    {
        //
    }
}

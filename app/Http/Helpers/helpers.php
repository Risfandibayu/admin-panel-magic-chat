<?php 

function getImage($image,$size = null, $isAvatar=false)
{
    $clean = '';
    $size = $size ? $size : 'undefined';
    if (file_exists($image) && is_file($image)) {
        return asset($image) . $clean;
    }elseif($isAvatar){
        return asset('public/assets/img/avatar.png');
    }else{
        return route('placeholderImage',$size);
    }
}
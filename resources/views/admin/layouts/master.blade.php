<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Dashboard</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="{{asset('public')}}/assets/img/Logo2.png" rel="icon">
    <link href="{{asset('public')}}/assets/img/Logo2.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.gstatic.com" rel="preconnect">
    <link
        href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
        rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{asset('public')}}/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{asset('public')}}/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="{{asset('public')}}/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="{{asset('public')}}/assets/vendor/quill/quill.snow.css" rel="stylesheet">
    <link href="{{asset('public')}}/assets/vendor/quill/quill.bubble.css" rel="stylesheet">
    <link href="{{asset('public')}}/assets/vendor/remixicon/remixicon.css" rel="stylesheet">
    <link href="{{asset('public')}}/assets/vendor/simple-datatables/style.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="{{asset('public')}}/assets/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/css/iziToast.min.css" integrity="sha512-O03ntXoVqaGUTAeAmvQ2YSzkCvclZEcPQu1eqloPaHfJ5RuNGiS4l+3duaidD801P50J28EHyonCV06CUlTSag==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <!-- =======================================================
  * Template Name: NiceAdmin
  * Updated: Jul 27 2023 with Bootstrap v5.3.1
  * Template URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
  @yield('style')
</head>

<body>

    <!-- ======= Header ======= -->
    @include('admin.layouts.header')

    <!-- ======= Sidebar ======= -->
    @include('admin.layouts.sidebar')

    @yield('content')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/js/iziToast.min.js" integrity="sha512-Zq9o+E00xhhR/7vJ49mxFNJ0KQw1E1TMWkPTxrWcnpfEFDEXgUiwJHIKit93EW/XxE31HSI5GEOW06G6BF1AtA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    @if(session()->has('notify'))
    @foreach(session('notify') as $msg)
        <script>
            'use strict';
            iziToast.{{ $msg[0] }}({message:"{{ $msg[1] }}", position: "topRight"});
        </script>
    @endforeach
    @endif

    <!-- ======= Footer ======= -->
    @include('admin.layouts.footer')

    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
            class="bi bi-arrow-up-short"></i></a>

    <!-- Vendor JS Files -->
    <script src="{{asset('public')}}/assets/vendor/apexcharts/apexcharts.min.js"></script>
    <script src="{{asset('public')}}/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="{{asset('public')}}/assets/vendor/chart.js/chart.umd.js"></script>
    <script src="{{asset('public')}}/assets/vendor/echarts/echarts.min.js"></script>
    <script src="{{asset('public')}}/assets/vendor/quill/quill.min.js"></script>
    <script src="{{asset('public')}}/assets/vendor/simple-datatables/simple-datatables.js"></script>
    <script src="{{asset('public')}}/assets/vendor/tinymce/tinymce.min.js"></script>
    <script src="{{asset('public')}}/assets/vendor/php-email-form/validate.js"></script>

    <!-- Template Main JS File -->
    <script src="{{asset('public')}}/assets/js/main.js"></script>
    @yield('script')

</body>

</html>
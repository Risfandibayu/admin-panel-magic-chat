<aside id="sidebar" class="sidebar">

    <ul class="sidebar-nav" id="sidebar-nav">

      <li class="nav-item">
        <a class="nav-link {{ $activeRoute === 'dashboard' ? '' : 'collapsed' }}" href="{{route('dashboard')}}">
          <i class="bi bi-grid"></i>
          <span>Dashboard</span>
        </a>
      </li><!-- End Dashboard Nav -->

      <li class="nav-item">
        <a class="nav-link {{ $activeRoute === 'mitra' ? '' : 'collapsed' }}" href="{{route('mitra')}}">
          <i class="bi bi-people"></i>
          <span>Data Mitra</span>
        </a>
      </li><!-- End Blank Page Nav -->

    </ul>

  </aside><!-- End Sidebar-->
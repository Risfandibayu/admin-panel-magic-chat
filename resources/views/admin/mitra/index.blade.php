@extends('admin.layouts.master')

@section('content')
<main id="main" class="main">

    <div class="pagetitle">
        <div class="row">
            <h1>Data Mitra</h1>
            <nav>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active">Data Mitra</li>
                </ol>
            </nav>

        </div>

    </div><!-- End Page Title -->

    <section class="section">
        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-body row">
                        <div class="col-6">
                            <h5 class="card-title">Datatables</h5>
                        </div>
                        <div class="col-6 p-2">
                            <button class="btn btn-primary float-end " data-bs-toggle="modal"
                                data-bs-target="#addModal"><i class="bi bi-plus"></i> Tambah Data</button>
                        </div>

                        <!-- Table with stripped rows -->
                        <table class="table datatable">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Nama</th>
                                    <th scope="col">Kode Referral</th>
                                    <th scope="col">Link Grup</th>
                                    <th scope="col">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($mitra as $item)


                                <tr>
                                    <th scope="row">{{$loop->iteration}}</th>
                                    <td>{{ $item->nama}}</td>
                                    <td>{{$item->ref_code}}</td>
                                    <td>{{$item->link_group}}</td>
                                    <td>
                                        <button class="btn btn-sm btn-warning edit" data-id={{$item->id}}
                                            data-nama="{{$item->nama}}"
                                            data-ref_code="{{$item->ref_code}}"
                                            data-link_group="{{$item->link_group}}"
                                            ><i class="bi bi-pencil"></i> Edit</button>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <!-- End Table with stripped rows -->

                    </div>
                </div>

            </div>
        </div>
    </section>


    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data Mitra</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </button>
                </div>
                <form action="{{route('mitra.store')}}" method="POST">
                    @csrf

                    <div class="modal-body">
                        <div class="row mb-3">
                            <label for="inputText" class="col-sm-2 col-form-label">Nama</label>
                            <div class="col-sm-10">
                                <input type="text" name="nama" class="form-control">
                            </div>
                        </div>
                        {{-- <div class="row mb-3">
                            <label for="inputEmail" class="col-sm-2 col-form-label">Kode Referral</label>
                            <div class="col-sm-10">
                                <input type="text" name="ref_code" class="form-control">
                            </div>
                        </div> --}}
                        <div class="row mb-3">
                            <label for="inputPassword" class="col-sm-2 col-form-label">Link Grup</label>
                            <div class="col-sm-10">
                                <input type="text" name="link_group" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Data Mitra</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </button>
                </div>
                <form action="{{route('mitra.update')}}" method="POST">
                    @csrf

                    <div class="modal-body">
                        <input type="hidden" name="id" class="id">
                        <div class="row mb-3">
                            <label for="inputText" class="col-sm-2 col-form-label ">Nama</label>
                            <div class="col-sm-10">
                                <input type="text" name="nama" class="form-control nama">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="inputEmail" class="col-sm-2 col-form-label ">Kode Referral</label>
                            <div class="col-sm-10">
                                <input type="text" name="ref_code" class="form-control kode" readonly>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="inputPassword" class="col-sm-2 col-form-label link">Link Grup</label>
                            <div class="col-sm-10">
                                <input type="text" name="link_group" class="form-control link">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>

            </div>
        </div>
    </div>

</main><!-- End #main -->
@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js" integrity="sha512-3gJwYpMe3QewGELv8k/BX9vcqhryRdzRMxVfq6ngyWXwo03GFEzjsUm8Q7RZcHPHksttq7/GFoxjCVUjkjvPdw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    "use strict";
        (function ($) {
            $('.edit').on('click', function () {
                console.log($(this).data('nama'));
                var modal = $('#editModal');
                modal.find('.id').val($(this).data('id'));
                modal.find('.nama').val($(this).data('nama'));
                modal.find('.kode').val($(this).data('ref_code'));
                modal.find('.link').val($(this).data('link_group'));
                modal.modal('show');
            });
        })(jQuery);
</script>
@endsection